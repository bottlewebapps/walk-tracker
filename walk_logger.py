#! /usr/bin/env python3

'''
Module to store walked steps per day
'''

import os
import sys
import json
import random
import datetime

# File where the data will be stored
data_file = './data.json'

# Date format "YEAR/MONTH/DAY"
date_format = '%Y/%m/%d'


def str2date(datestr):
    '''
    Convert "date" strings into datetime objects
    '''
    d = datetime.datetime.strptime(datestr, date_format)
    return d


def date2str(date) -> str:
    '''
    Convert a datetime (day) object into a string (i.e. "2022/03/31")
    '''
    datestr = datetime.datetime.strftime(date, date_format)
    return datestr


now = datetime.datetime.now()
today_str = date2str(datetime.datetime.now())


def read_steps() -> dict:
    if os.path.isfile(data_file):
        # Ensure that the file is not empty
        if os.stat(data_file).st_size > 0:
            with open(data_file) as fh:
                data = json.load(fh)
            if data:
                return data
    # If the file does not exist or has no data return an empty dictionary
    return {}


def store_steps(datestr: str, steps: int, reset: bool = True) -> None:
    '''
    Function to store steps in a JSON dictionary.
    If the given date already exists the given steps value will overwrite
    the previous value. If "reset" is set to "False", the steps will be added
    to the previous count.
    '''
    data = read_steps()
    if datestr not in data or reset:
        data[datestr] = steps
    else:
        # The date exists and reset=False => add
        data[datestr] = data[datestr] + steps

    with open(data_file, 'w', encoding='utf-8') as fh:
        json.dump(data, fh, indent=4)


# r = read_steps()
# print("Stored steps 0", r)
# store_steps(today_str, 10, False)

# r = read_steps()
# print("Stored steps 1", r)


def date_within(date_str: str, n_days: int) -> bool:
    '''
    Check whether a given date string is within the last "n_days" from now.
    '''
    d = str2date(date_str)
    dt = now - d
    if dt.days < n_days:
        return True
    return False


def date_current_month(date_str: str) -> bool:
    '''
    Check whether a given date string is within the current month.
    '''
    d = str2date(date_str)
    if d.year == now.year and d.month == now.month:
        return True
    return False


def step_stats() -> dict:
    data = read_steps()
    stats = {}
    stats['steps_today'] = data[today_str] if today_str in data else 0

    tmp_week, tmp_month = 0, 0
    for date, steps in data.items():
        if date_within(date, 7):
            tmp_week += steps

        if date_current_month(date):
            tmp_month += steps

    stats['steps_week'] = tmp_week
    stats['steps_month'] = tmp_month

    return stats


def delete_data() -> None:
    '''
    Delete all stored data
    '''
    with open(data_file, 'wt') as fh:
        fh.write('{}')


def demo_data() -> None:
    '''
    Generate some random data
    '''
    # Last 7 days
    days = []
    for d in range(7):
        di = datetime.datetime.today() - datetime.timedelta(days=d)
        days.append(date2str(di))
    # Add a couple of days within the last month
    nsamples = 10
    for d in random.sample(range(7, 30), 10):
        di = datetime.datetime.today() - datetime.timedelta(days=d)
        days.append(date2str(di))

    data = {}
    for d in days:
        random_steps = random.randint(0, 15000)
        store_steps(d, random_steps)


usage = '''
In order to use the "walk_logger.py" module interactively,
there are two options:
1. Without arguments it dumps all the stored values
2. With a date (string) and a number of steps (integer), it stored them. i.e.:
./walk_logger.py '2022/03/31' 5740
'''

if __name__ == '__main__':
    if len(sys.argv) == 1:
        data = read_steps()
        print('Walked steps per day:')
        for d, s in data.items():
            print(' ' + str(d) + ': ' + str(s))

    elif len(sys.argv) == 3:
        datestr = sys.argv[1]
        steps = sys.argv[2]
        # Perform some minimal validation of the data
        if '/' not in datestr or not steps.isdigit():
            print(usage)
            sys.exit(1)
        # Store the values
        store_steps(datestr, int(steps))
    else:
        print(usage)
        sys.exit(1)
