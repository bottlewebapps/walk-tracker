# Walk tracker

A simple Python/Bottle webapp to store the steps we walk every day and visualize
 some statistics.

It depends on matplotlib to generate a graph with the steps of the last 7 days.


# How to use
Install the Python dependencies by running the following command on a terminal:

    python -m pip install -r requirements.txt -U

Run the app from a terminal by typing:

    python app.py

This will start the webapp daemon and keep showing new lines every time we visit the webapp. Open a browser (i.e. Firefox) and visit the address <http://localhost:8080>. If everything worked out, you should see the webapp.
