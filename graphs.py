#! /usr/bin/env python3

'''
Module to generate graphs using the data.
If run interactivelly opens an matplotlib window with the graph,
otherwise it stores the graph as an png file in the "static" folder.
'''


import os
import matplotlib
import matplotlib.pyplot as plt
from datetime import datetime

import walk_logger


def prepare_data(data: dict, n_days: int = 7):
    '''
    Take the last 7 days of data and return it as two softed lists
    '''
    tmp = {}
    for date, steps in data.items():
        if walk_logger.date_within(date, n_days):
            tmp[date] = steps

    keys = list(tmp.keys())
    keys.sort()
    values = [tmp[k] for k in keys]
    points = (keys, values)
    return points


def gen_plot(graph_file: str = './static/steps_graph.png', n_days: int = 7):
    data = walk_logger.read_steps()
    points = prepare_data(data, n_days)

    x = [walk_logger.str2date(d) for d in points[0]]
    formatter = matplotlib.dates.DateFormatter(walk_logger.date_format)
    b = points[1]

    figure = plt.figure()
    axes = figure.add_subplot(1, 1, 1)

    axes.xaxis.set_major_formatter(formatter)
    plt.setp(axes.get_xticklabels(), rotation=15)

    plt.suptitle('Steps walked in the last ' + str(n_days) + ' days')
    plt.xlabel('Date')
    plt.ylabel('Steps')
    # axes.plot(x, b)
    axes.bar(x, height=b)

    if graph_file:
        plt.savefig(graph_file, bbox_inches='tight')
    else:
        plt.show()


if __name__ == '__main__':
    gen_plot(graph_file=None)
