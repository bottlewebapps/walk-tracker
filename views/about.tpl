<!DOCTYPE html>
<!-- This is an HTML comment -->
<head>
    <title>Walk tracker app: about the app</title>
</head>

<body>

<a href="/">Back to main page</a>

<h1>Walk tracker app</h1>

<p>This is a basic webapp written in Python, using the Bottle framework.</p>

<p>If we enter the amount of steps we walked every day, it will show us statistics of our exercise.</p>


<br>
<br>

<div>
    <span style="float: right;">
        If used for evaluation, here you can <a href="?delete_data=true">delete all the data</a> <br>
        or <a href="?demo_data=true">generate some random demo data</a>.
    </span>
</div>


</body>

