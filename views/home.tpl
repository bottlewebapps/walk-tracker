<!DOCTYPE html>
<!-- This is an HTML comment -->
<head>
    <title>Walk tracker app</title>
    <style>
#stats {
  overflow: hidden;
}

#stats div {
  float: left;
  width: 120px;
  background-color: lightblue;
  margin: 5px;
  border: 4px solid blue;
}

p { text-align: center; }

#progressbar {
    height: 20px;
    width: 200px;
    text-color: black;
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
 }

#steps {
    font-size: 24px;
    width: 150px;
}
    </style>
</head>

<body>

<form>
  <h2>
  <label for="steps">Today {{today_str}} I walked </label>
  <input type="number" id="steps" name="steps" value="{{stats['steps_today']}}">
  steps.
  <button type="submit">Submit</button>
  </h2>
</form>

%if stats:
  <div id="stats">
  % for k, v in stats.items():
    <div>
      <p>{{k.replace('_', ' ').capitalize()}}</p>
      <p>{{v}}</p>
    </div>
  % end

  </div>

<br>


  <label for="progressbar">Daily progress:</label>
  <progress id="progressbar" max="10000" value="{{stats['steps_today']}}">{{stats['steps_today']}} steps from 10000</progress>

  <img src="static/steps_graph.png" alt="Graph with the steps walked on the last days">

%end

<span style="float: right;">
<a href="/about">About</a>
</span>

</body>
