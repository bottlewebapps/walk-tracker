#! /usr/bin/env python3

from bottle import route, run, template, request, redirect, static_file

# Our own modules
import walk_logger
import graphs


@route('/static/<filename:re:.*.(svg|png)>')
def static(filename):
    return static_file(filename, root='./static/')


@route('/', method="GET")
def home():
    getdata = dict(request.query.decode())
    if 'steps' in getdata and getdata['steps'].isdigit():
        steps = int(getdata['steps'])
        walk_logger.store_steps(walk_logger.today_str, steps)

    data = {}
    data['today_str'] = walk_logger.today_str
    data['stats'] = walk_logger.step_stats()
    if data['stats']:
        graphs.gen_plot()

    return template('home', **data)


@route('/about', method="GET")
def about():
    getdata = dict(request.query.decode())
    if 'demo_data' in getdata and getdata['demo_data']:
        print('Generating some demo data')
        walk_logger.demo_data()
    elif 'delete_data' in getdata and getdata['delete_data']:
        print('Deleting all stored data!!')
        walk_logger.delete_data()

    return template('about')


run(host='127.0.0.1', port=8080, debug=True, reloader=True)
